﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigEnemy : MonoBehaviour
{
    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private bool canShoot;

    private Vector2 pos;
    [SerializeField]
    private float bulletForce;

    [SerializeField]
    private int health;

    // Start is called before the first frame update
    void Start()
    {
        canShoot = true;
        health = 5;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if (GameObject.Find("Canvas").GetComponent<UIController>().isStarted)
        {
            pos = transform.position;
            pos.y -= 0.005f;
            transform.position = pos;

            Shoot();

            CheckIfOut();
        }
    }

    private void Shoot()
    {
        if (canShoot)
        {
            var shoot = Instantiate(bullet, transform.position + new Vector3(0.55f, -0.8f, 0), Quaternion.identity);
            shoot.GetComponent<Rigidbody2D>().AddForce(Vector2.down * bulletForce, ForceMode2D.Impulse);
            shoot.transform.up = Vector2.up;
            shoot = Instantiate(bullet, transform.position + new Vector3(-0.5f, -0.8f, 0), Quaternion.identity);
            shoot.GetComponent<Rigidbody2D>().AddForce(Vector2.down * bulletForce, ForceMode2D.Impulse);
            shoot.transform.up = Vector2.up;
            StartCoroutine(ShootCooldown());
        }
    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(1.5f);
        canShoot = true;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || collision.CompareTag("Shoot"))
        {
            if (collision.CompareTag("Shoot"))
            {
                Destroy(collision.gameObject);
            }
            health--;
            if (health <= 0)
            {
                DestroyBigEnemy();
            }
            StartCoroutine(HurtAnimation());
        }
    }

    private IEnumerator HurtAnimation()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(1f);
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    void CheckIfOut()
    {
        if (transform.position.y < -6)
        {
            Destroy(gameObject);
        }
    }

    void DestroyBigEnemy()
    {
        GameObject.Find("Canvas").GetComponent<UIController>().IcreaseScore(20);
        Destroy(gameObject);
    }
}
