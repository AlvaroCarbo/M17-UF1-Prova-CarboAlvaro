﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    [SerializeField]
    private GameObject bigEnemy;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("InvokeEnemy", 1, 2);
        InvokeRepeating("InvokeBigEnemy", 1, 10);
    }

    void InvokeEnemy()
    {
        if (GameObject.Find("Canvas").GetComponent<UIController>().isStarted)
        {
            var instance = Instantiate(enemy, new Vector3(Random.Range(-10, 10), 10, 0), Quaternion.identity);
            instance.transform.up = Vector2.down;
        }
    }

    void InvokeBigEnemy()
    {
        if (GameObject.Find("Canvas").GetComponent<UIController>().isStarted)
        {
            var instance = Instantiate(bigEnemy, new Vector3(Random.Range(-9, 9), 13, 0), Quaternion.identity);
            instance.transform.up = Vector2.up;
        }
    }
}
