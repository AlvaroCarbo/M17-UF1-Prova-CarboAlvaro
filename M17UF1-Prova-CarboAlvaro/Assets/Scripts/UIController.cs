﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private Text scoreText;
    private float score;

    public bool isStarted;

    [SerializeField]
    private GameObject startPanel;
    [SerializeField]
    private Button button;
    // Start is called before the first frame update
    void Start()
    {
        isStarted = false;
        score = 0;
        button.onClick.AddListener(StartGame);
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = $"Score: {score}";
    }

    public void IcreaseScore(float scoreToAdd)
    {
        score += scoreToAdd;
    }

    private void StartGame()
    {
        startPanel.SetActive(false);
        button.gameObject.SetActive(false);
        isStarted = true;
    }
}
