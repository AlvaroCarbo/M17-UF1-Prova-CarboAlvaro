﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private bool canShoot;

    private Vector2 pos;
    [SerializeField]
    private float bulletForce;

    // Start is called before the first frame update
    void Start()
    {
        canShoot = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if (GameObject.Find("Canvas").GetComponent<UIController>().isStarted)
        {
            //pos = transform.position;
            //pos.y -= 0.01f;
            //transform.position = pos;
            transform.position = Vector3.MoveTowards(transform.position, GameObject.Find("PlayerShip").transform.position, 0.01f);

            Shoot();

            CheckIfOut();
        }
    }

    private void Shoot()
    {
        if (canShoot)
        {
            var shoot = Instantiate(bullet, transform.position, Quaternion.identity);
            shoot.GetComponent<Rigidbody2D>().AddForce(Vector2.down * bulletForce, ForceMode2D.Impulse);
            shoot.transform.up = Vector2.up;
            StartCoroutine(ShootCooldown());
        }
    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(2.5f);
        canShoot = true;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || collision.CompareTag("Shoot"))
        {
            Destroy(gameObject);
            GameObject.Find("Canvas").GetComponent<UIController>().IcreaseScore(5);
            if (collision.CompareTag("Shoot"))
            {
                Destroy(collision.gameObject);
            }
        }
    }

    void CheckIfOut()
    {
        if (transform.position.y < -4)
        {
            Destroy(gameObject);
        }
    }
}
