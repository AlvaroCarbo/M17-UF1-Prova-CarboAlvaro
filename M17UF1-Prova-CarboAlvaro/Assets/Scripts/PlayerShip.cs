﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerShip : MonoBehaviour
{
    [SerializeField]
    private float velocity;
    [SerializeField]
    private Vector2 inputDirection;
    [SerializeField]
    private bool spaceInput;
    [SerializeField]
    GameObject bullet;
    [SerializeField]
    private float bulletForce;
    private bool canShoot;
    [SerializeField]
    private int shootN;


    // Start is called before the first frame update
    void Start()
    {
        canShoot = true;
    }

    // Update is called once per frame
    void Update()
    {
        inputDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        spaceInput = Input.GetKey(KeyCode.Space);
    }

    void FixedUpdate()
    {
        if (GameObject.Find("Canvas").GetComponent<UIController>().isStarted)
        {
            Move();

            Shoot();
        }

    }

    void Move()
    {
        if (inputDirection.x != 0 || inputDirection.y != 0)
        {
            Vector2 moveDirection = Vector2.ClampMagnitude(inputDirection, 1);
            GetComponent<Rigidbody2D>().AddForce(moveDirection * velocity, ForceMode2D.Force);
        }
    }


    void Shoot()
    {
        if (canShoot)
        {
            if (spaceInput)
            {
                var shoot = Instantiate(bullet, transform.position, Quaternion.identity);
                shoot.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bulletForce, ForceMode2D.Impulse);
                shoot.transform.up = Vector2.down;
                StartCoroutine(ShootCooldown());
            }
        }
    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(0.15f);
        shootN++;
        if (shootN >= 5)
        {
            yield return new WaitForSeconds(0.5f);
            shootN = 0;
        }
        canShoot = true;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("EnemyShoot"))
        {
            SceneManager.LoadScene(0);
        }
    }
}
